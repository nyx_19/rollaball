﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public int playerspeed = 10; 
    //from the code nathan showed me to shrink player. above was the pre-existing speed variable from roll a ball

    //public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject conditionGameObject;
    public GameObject player;

    private Rigidbody rb;
    private int size;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player.transform.localScale = new Vector3(1, 1, 1);
        //SetCountText();
        winTextObject.SetActive(false);
        conditionGameObject.SetActive(true);
        
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }
    
   // void SetCountText() will delete once i feel confident everything wont break
    //{
        //countText.text = "Count: " + count.ToString();
        //if(count >= 12)
        //{
          //  winTextObject.SetActive(true);
        //}
    //}

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * playerspeed);

    }

    void Update ()
        //not the same as fixed update from the example so we're just going to make a sepperate one for you
    {
        Vector3 size = transform.localScale;

        size = Vector3.Lerp(size, Vector3.zero, Time.deltaTime * speed);

        transform.localScale = size;

        }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup")) 
        {
           other.gameObject.SetActive(false);
            transform.localScale += new Vector3(1, 1, 1);
            //makes the player grow. currently working
            //count = count + 1;

            //SetCountText()
        }
        if (other.gameObject.CompareTag("End"))
         {
            winTextObject.SetActive(true);
        }
        
    }
}
